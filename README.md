[[_TOC_]]
# install_glove80_uf2

## Introduction

`install_glove80_uf2` is a Linux tool that simplifies updating the firmware on your [MoErgo](https://www.moergo.com/) Glove80 keyboard.

The script handles mounting, flashing, and unmounting the keyboard units.

If both keyboard units are connected, it will flash both units in the same invokation.

A UF2 file can be used, but for convenience if you don't specify a file it will pick the `*.uf2` with the latest timestamp in the given directory (defaults to the current working directory).


## Installation

`install_glove80_uf2` is a single file that you can clone/download and place/link in directory on your `${PATH}`, e.g `~/.local/bin`, and make sure it's executable.  e.g.:

    curl -o ~/.local/bin/install_glove80_uf2 "https://gitlab.com/eBardie/install_glove80_uf2/-/raw/main/install_glove80_uf2?ref_type=heads&inline=false"
    chmod 755 ~/.local/bin/install_glove80_uf2

er:

    git clone https://gitlab.com/eBardie/install_glove80_uf2.git
    ln -s $(realpath install_glove80_uf2)/install_glove80_uf2 ~/.local/bin/
    chmod 755 ~/.local/bin/install_glove80_uf2

IMPORTANT:  This script must run with root privileges.  Please read through the code to make sure it's not doing anything you don't want it to.  Also, please send feedback/PRs :)

An alternative location might be which ever directory you download your UF2 files to.  If this isn't on your `${PATH}`, and why would it be?, you'll need to run the script with full or relative path name, e.g. `sudo ./install_glove80_uf2`.

The script makes use of various Unix commands, but they're likely already installed.  When you run it, the script checks that it can see the programmes it needs, and it will let you know if it can't, and then exit early.

Check that the script is happy by having it display its help info:

    install_glove80_uf2 --help

You should see something like:

```
usage:

   install_glove80_uf2 [<options>] [<UF2_FILEPATH>]

options:

  -d <UF2_DIR> | --directory <MUF2_DIR> : path to directory containing UF2
                                          files (default: ${PWD})
  -h | --help : display this helpful information
  -m <MOUNTPOINT> | --mountpoint <MOUNTPOINT> : path to use as a mountpoint

notes:

If no UF2_FILEPATH is given, the programme will try to use the latest UF2 image
file, if any, in the specified directory, if one has been specified, or else
the current directory.

  The programme must be run as the root user e.g. sudo install_glove80_uf2
```


## Usage

You have created your new firmware, presumeably over at the [Glove80 Layout Editor](https://my.glove80.com/), and downloaded the resulting UF2 file.


### Step 1: connect the keyboard unit(s)

Connect one or both keyoard units to the Linux computer using fully capable USB-C cables.  Full capability here means that the cable can transfer data and isn't one of those PITA charging only cables 😉


### Step 2: make sure they're turned on

Are the units' power buttons in the on position?


### Step 3: put the unit(s) into bootloader mode

See the official [manual](https://docs.moergo.com/glove80-user-guide/customizing-key-layout/#putting-glove80-into-bootloader-for-firmware-loading).

NOTE: If you're flashing both units, remember to put the secondary unit into bootloader mode before the primary unit so you can still press the Magic key :)


### Step 4: run the command

The following commands will install the UF2 file with the latest timestamp in the directory.

NOTE: `install_glove80_uf2` needs to be run as `root`, e.g usign `sudo install_glove80_uf2`.  If you forget, it will remind you.


In a terminal shell, navigate to the directory containing your UF2 file(s), and run `install_glove80_uf2`:

    cd ~/Downloads/glove80
    sudo install_glove80_uf2

Alternative:

    sudo install_glove80_uf2 -d ~/Downloads/glove80


If want instead to specify a particular UF2 file:

    cd ~/Downloads/glove80
    sudo install_glove80_uf2 specific_file.uf2

or:

    sudo install_glove80_uf2 ~/Downloads/glove80/specific_file.uf2


You should see something like the followuing:

```
Using latest UF2 file in '/home/ebardie/Downloads/glove80/':  '/home/ebardie/Downloads/glove80/ebardie 0.10 based on Advanced Home Row Mods (HRM) Example v35.uf2'
Mounting '/dev/sdc (GLV80LHBOOT)' on '/tmp/install_glove80_tmpmnt_385391'
Mounting '/dev/sdb (GLV80RHBOOT)' on '/tmp/install_glove80_tmpmnt_385391'
Done
```




